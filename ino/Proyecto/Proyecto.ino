#include <Chrono.h>
#include <LightChrono.h>

#include <Servo.h>

const int LDRPin = A0;   //Pin del LDR
const int ServoPin = A1;
const int LEDPin = A2;
const Servo MyServo;
long minLuminosidad = 20;
long maxLuminosidad = 75;
bool setServo = false;
bool setLED = false;

const int SET_RANGO_FUNC_ID = 1;
const int GET_RANGO_FUNC_ID = 2;
const int GET_ESTADO_FUNC_ID = 3;
const int SET_LED = 4;
const int SET_SERVO = 5;
const int SET_LED_BY_TIME = 6;
const int SET_SERVO_BY_TIME = 7;
const int GET_LUMINOSIDAD_BY_INTERVAL = 8;

bool tapaAbierta, ledEncendido;
long luminosidad;

void setup()
{
  Serial.begin(9600);
  MyServo.attach(ServoPin);
  operarTapa(false);
  pinMode(LEDPin, OUTPUT);
  operarLED(false);
  delay(1000);
}

void loop() {
  procesarSiguienteComando();
  autoLoop();
}

void autoLoop()
{
  luminosidad = analogRead(LDRPin);
  //Serial.print("Luminosidad: ");
  //Serial.println(luminosidad);
  //Serial.print("LED: ");
  //Serial.println(digitalRead(LEDPin) == LOW ? "ON" : "OFF");
  if (!setServo && !setLED) {
    if (luminosidad < minLuminosidad) {
      if (tapaAbierta) {
        //Serial.println("Luminosidad por debajo del minimo: encendiendo LED.");
        operarLED(true);
      } else {
        //Serial.println("Luminosidad por debajo del minimo: abriendo tapa.");
        operarTapa(true);
      }
    } else {
      if (luminosidad > maxLuminosidad) {
        if (ledEncendido) {
          //Serial.println("Luminosidad por encima del minimo: apagando LED.");
          operarLED(false);
        } else {
          //Serial.println("Luminosidad por encima del minimo: cerrando tapa.");
          operarTapa(false);
        }
      }
    }
    delay(1000);
  } else {
    if (setServo) {
    }
    if (setLED) {
    }
  }
}

int procesarSiguienteComando() {
  String comando = Serial.readStringUntil(';');
  int indiceDelimitadorComando = comando.indexOf(':');
  int idComando = comando.substring(0, indiceDelimitadorComando).toInt();
  String params = comando.substring(indiceDelimitadorComando + 1, comando.length() - 1);
  ejecutarComando(idComando, params);
}

void ejecutarComando(int id, String params) {
  String reqParams;
  switch (id) {
    case (SET_RANGO_FUNC_ID):
      int indiceDelimitadorParametros = params.indexOf(',');
      int minRango = params.substring(0, indiceDelimitadorParametros).toInt();
      int maxRango = params.substring(indiceDelimitadorParametros + 1, params.length()).toInt();
      minLuminosidad = minRango;
      maxLuminosidad = maxRango;
      Serial.println("ok");
      break;
    case (GET_RANGO_FUNC_ID):
      reqParams = String(minLuminosidad) + ',' + String(maxLuminosidad);
      Serial.println("algo");
      Serial.println(reqParams);
      break;
    case (GET_ESTADO_FUNC_ID):
      reqParams = armarJSONMediciones();
      Serial.println(reqParams);
      break;
    case (SET_LED):
      break;
    case (SET_SERVO):
      break;
    case (SET_LED_BY_TIME):
      break;
    case (SET_SERVO_BY_TIME):
      break;
    case (GET_LUMINOSIDAD_BY_INTERVAL):
      break;
  }
}

void operarLED(bool val) {
  digitalWrite(LEDPin, val ? LOW : HIGH);
  ledEncendido = val;
  delay(1000);
}

void operarTapa(bool val) {
  MyServo.write(val ? 180 : 90);
  tapaAbierta = val;
  delay(1000);
}

String armarJSONMediciones() {
  return "{\"led\": " + boolToStr(ledEncendido) + ", \"ldr\": " + luminosidad + "\"tapaAbierta\": " + boolToStr(tapaAbierta) + "}";
}

String boolToStr(bool val) {
  return val ? "true" : "false";
}
