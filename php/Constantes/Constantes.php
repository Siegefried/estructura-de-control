<?php

abstract class Constantes
{
    const formatoFecha = 'Y-m-d H:i:s';
    const archivoEstadoComponentes = 'estadoComponentes.json';
    const formatoIntervalo = '%y años %m meses %a días %h horas %i minutos %s segundos';
    const puertoSerial = "/dev/ttyACM0";
    const buffer = 100;
}