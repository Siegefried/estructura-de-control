
<?php

require_once "Constantes/Constantes.php";
require_once "Constantes/Servicios.php";
require_once "Constantes/Data.php";

function diferenciaFechaHora(DateTime $anterior){
    $ahora = new DateTime();
    $diferencia = $ahora->diff($anterior);
    echo $diferencia->format(Constantes::formatoIntervalo)."\n";
}

function obtenerInputSetRango() : array {
    exec("tput reset");
    $min = "min";
    $max = "max";
    $resultado = array(
        $min => "",
        $max => ""
    );
    $valido = false;
    while( !$valido ){
        $resultado[$min] = readline("Ingrese el mínimo del rango de control : ");
        $valido = is_numeric($resultado[$min]);
        if( !$valido ){
            echo "ERROR: el mínimo debe ser un número\n";
        }
    };
    $valido = false;
    while( !$valido ){
        $resultado[$max] = readline("Ingrese el máximo del rango de control : ");
        $valido = is_numeric($resultado[$max]);
        if( !$valido ){
            echo "ERROR: el máximo debe ser un número\n";
        }
    };
    return $resultado;
}

function setRango(array $rango) {

    $min = "min";
    $max = "max";
    $puertoSerial = fopen(
        Constantes::puertoSerial,
        'w'
    );
    fwrite(
        $puertoSerial,
        Servicios::setRango.':'.$rango[$min].','.$rango[$max].";"
    );
    fclose($puertoSerial);
}

function testSetRango(){

    sleep(1);
    $contenido = leerSerial();
    echo "Enviado al puerto serial: ".$contenido."\n";
}

function testGetRango(){

    sleep(1);
    escribirSerial('2:2,4;');
}

function testGetEstado() {
    $json = "4:".file_get_contents(Constantes::archivoEstadoComponentes).";";
    escribirSerial($json);
}

function getRango() : array{
    $serial = leerSerial();
    $min = "min";
    $max = "max";
    $rango = array(
        $min => "",
        $max => ""
    );
    $rango[$max] = substr_replace(explode(",", $serial)[1] ,"", -1);
    $rango[$min] = explode(",",explode(":", $serial)[1])[0];
    echo "Rango de Control actual: \n";
    echo "Mínimo: ".$rango[$min]."\n";
    echo "Máximo: ".$rango[$max]."\n";
    return $rango;
}

function getEstado() {
    $serial = leerSerial();
    $serial = explode(";", $serial)[0];
    $serial = explode(":", $serial, 2)[1];
    $json = json_decode($serial, true);

    echo "Led: ".$json[Data::led]."\n";
    echo "Tapa Abierta: ".$json[Data::tapaAbierta]."\n";
    echo "Ldr: ".$json[Data::ldr]."\n";
}

function testearEstadoActualComponentes(){

    $puertoSerial = fopen(Constantes::puertoSerial,"w");

    while(true){
        //sleep(3);
        exec("tput reset");
        var_dump(fwrite($puertoSerial, file_get_contents(Constantes::archivoTest)));
    }
}

function imprimirMenu(){
    echo "Sistemas de control Laboratorio 2\n";
    echo "1- Tiempo desde última comunicación\n";
    echo "2- Establecer intervalo de control\n";
    echo "3- Obtener intervalo de control actual\n";
    echo "4- Obtener información de estado actual de los componentes\n";
    echo "5- Encender/apagar actuador\n";
    echo "6- Encender actuador por tiempo\n";
    echo "7- Obtener sensores por tiempo\n";
}

function leerSerial() : string{
    $puertoSerial = fopen(
        Constantes::puertoSerial,
        'r'
    );

    $contenido = fread(
        $puertoSerial,
        Constantes::buffer
    );

    fclose($puertoSerial);
    return $contenido;
}

function escribirSerial(string $texto) {

    $puertoSerial = fopen(
        Constantes::puertoSerial,
        'w'
    );

    $contenido = fwrite(
        $puertoSerial,
        $texto
    );

    fclose($puertoSerial);
}
