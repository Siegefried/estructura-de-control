<?php

require_once "funciones.php";

$opcion = -1;
const opcion_min = 0;
const opcion_max = 6;

$ultima_comunicacion = new DateTime();

do{
    exec("tput reset");
    imprimirMenu();
    $valido = false;
    while( !$valido ){
        $opcion = readline("Ingrese una opción : ");
        $valido = is_numeric($opcion) && $opcion >= opcion_min && $opcion <= opcion_max;
        if( !$valido ){
            echo "ERROR: opción inválida o inexistente\n";
        }
    };
    switch ($opcion){
        case Servicios::uptime:
            diferenciaFechaHora($ultima_comunicacion);
            break;
        case 2:
            setRango(obtenerInputSetRango());
            testSetRango();
            break;
        case 3:
            testGetRango();
            getRango();
            break;
        case 4:
            testGetEstado();
            getEstado();
            break;
        case 5:

            break;
        case 6:

            break;
        case 7:

            break;
    }

}while($opcion );